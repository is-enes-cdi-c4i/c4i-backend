# C4I Backend

## Start c4i-backend via docker-compose

*Pre-requisites:* Install Docker and docker-compose and make sure you can access the docker deamon as local user ([manual for installation](https://docs.docker.com/v17.09/engine/installation/linux/docker-ce/ubuntu/#set-up-the-repository); [manual for post installation steps](https://docs.docker.com/install/linux/linux-postinstall/). )

To start c4i-backend via docker-compose make the following steps:
- clone this repository and cd into c4i-backend Docker folder
```
cd c4i-backend
```
- An `.env` environment file is used by docker-compose.yml. A default file can be generated with the command `bash docker-compose-generate-env.sh`. This will setup the default settings for your services/networks/volumes.
```
bash docker-compose-generate-env.sh 
cat .env
```
- Create the listed directories (ADAGUC_DATA_DIR, ADAGUC_AUTOWMS_DIR, ADAGUC_DATASET_DIR, ADAGUCSERVICES_DIR) before starting adaguc:
```
source .env && sudo mkdir -p ${ADAGUC_DATA_DIR} ${ADAGUC_AUTOWMS_DIR} ${ADAGUC_DATASET_DIR} ${ADAGUCSERVICES_DIR}
```
- With `docker-compose up` the application is build, (re)created, started, and attached to containers for a service.
```
docker-compose up --build
```


### Using the basket 
To initialize a user for the basket, first make a listing of it by calling it's API, this will initialize the user `myuser`. In the default configuration you are signed in as myuser, you don't have to sign in.
- Make a basket listing: ``curl -kL https://`hostname`/adaguc-services/basket/list && echo``
- Test the basket: put a TXT file in the basket:

  - From the Docker directory source the environment file, this will set the right environment variables: `source .env`
  - By default the directories are owned by root, change this via `sudo chown $USER: ${ADAGUCSERVICES_DIR}/ -R`
  - Put a file in the basket: `echo "hello!" > ${ADAGUCSERVICES_DIR}/myuser/data/hello.txt`
  - List the basket again: ``curl -kL https://`hostname`/adaguc-services/basket/list | python -m json.tool``
  - Download the file: ``curl -kL https://`hostname`/adaguc-services//opendap/myuser/hello.txt``

  
- Put a NetCDF file in the basket:
  - Download NC file and put it in the basket: `curl -L http://opendap.knmi.nl/knmi/thredds/fileServer/IS-ENES/TESTSETS/tasmax_day_EC-EARTH_rcp26_r8i1p1_20060101-20251231.nc > ${ADAGUCSERVICES_DIR}/myuser/data/netcdf.nc`
  - List basket: ``curl -kL https://`hostname`/adaguc-services/basket/list | python -m json.tool``
  - ncdump the file: ``ncdump -h https://`hostname`/adaguc-services/opendap/myuser/netcdf.nc``
  - ncview the file: ``ncview https://`hostname`/adaguc-services/opendap/myuser/netcdf.nc``


### Getting metadata and previews for files served via OpenDAP  

Adaguc-server is connected to adaguc-services via the wms? servlet endpoint. You can use adaguc-server to list metadata, use the Web Coverage Service (WCS) to download, regrid and reformat the data, use the Web Map Service (WMS) to visualize the data. The WMS, WCS and METADATA endpoints are:
- ```https://`hostname`/adaguc-services/wms?source=${encodedfilename}&service=wms&```
- ```https://`hostname`/adaguc-services/adagucserver?source=${encodedfilename}&service=wcs&```
- ```https://`hostname`/adaguc-services/adagucserver?source=${encodedfilename}&service=metadata&```

For example:
  - Get URLEncode for bash: `curl -L https://gist.githubusercontent.com/cdown/1163649/raw/356166e6a1564d93e02e174718eb59f50108a7aa/gistfile1.sh > /tmp/urlencode.sh` and activate it: `source /tmp/urlencode.sh`
  - Encode the OpenDAP URL: ```encodedfilename=`urlencode "https://\`hostname\`/adaguc-services//opendap/myuser/netcdf.nc"` && echo $encodedfilename```
  - With the encoded URL we can visualize the file and obtain metadata of the file
  - ```echo https://`hostname`/adaguc-services/wms?source=${encodedfilename}``` You can use this URL in https://geoservices.knmi.nl/viewer3.0/ to visualize the data. You can also use this URL in the react-webmapjs tools.
  - WMS GetCapabilities:    ``curl -kL "https://`hostname`/adaguc-services/wms?source=${encodedfilename}&service=wms&request=getcapabilities"`` 
  - WCS GetCapabilities:    ``curl -kL "https://`hostname`/adaguc-services/wms?source=${encodedfilename}&service=wcs&request=getcapabilities"``
  - WMS GetMap for preview: ``curl -kL "https://`hostname`/adaguc-services/wms?source=${encodedfilename}&service=wms&request=getmap&width=1000&format=image/png&LAYERS=tasmax&title=ECEarth%20Tasmax%20test&CRS=EPSG:4326&" > /tmp/test.png && display /tmp/test.png``
  - Get metadata descriptions about dimensions, variables and their attributes: ``curl -kL "https://`hostname`/adaguc-services/wms?service=metadata&source=${encodedfilename}&request=getmetadata"``


# Installation on AWS EC2

One time setup on a fresh EC2 instance:
- SSH into the EC2 instance
- sudo yum -y install git docker 
- sudo service docker start
- sudo usermod -a -G docker ec2-user
- sudo systemctl enable docker
- sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-`uname -s`-`uname -m` | sudo tee /usr/local/bin/docker-compose > /dev/null
- sudo chmod +x /usr/local/bin/docker-compose
- sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
- exit and ssh again into the ec2-instance

Install the c4i-backend on the EC2 instance
- git clone https://gitlab.com/is-enes-cdi-c4i/c4i-backend.git
- cd c4i-backend/
- git submodule update --init --recursive
- bash docker-compose-generate-env.sh -e "https://\<public ip of your instance ec2\>/"
- docker-compose up -d --build

To update:
- docker-compose down
- git submodule update --recursive --remote
- docker-compose up -d --build

## Updating backend modules in manual fashion in gitlab repository

This procedure will update the reference from c4i-backend repository into c4i-search-portal-backend repository:
- [localhost is-enes-c4i]$ git clone --recursive git@gitlab.com:is-enes-cdi-c4i/c4i-backend.git
- cd c4i-backend
- git submodule update --recursive --remote
- cd c4i-search-portal-backend
- git show -s && git branch -a
- cd ..
- git status -uno
- git commit -am "refreshing the c4i-search-portal-backend reference"
- git push origin master

-
