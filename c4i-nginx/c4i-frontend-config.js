var config = {};

config.frontendContentURL = './content/';
config.c4iSearchPortalBackend = 'https://34.254.195.81/c4i-search-portal-backend/';
config.catalogBackendUrl = 'https://34.254.195.81/adaguc-services/';
config.hostnameUrl = config.catalogBackendUrl + 'wms?source=';
config.WMSserviceUrl = '&service=wms&request=getcapabilities';
config.WCSserviceUrl = '&service=wcs&request=getcapabilities';
config.MAPserviceUrl = '&service=wms&request=getmap&width=1000&format=image/png&LAYERS=tasmax&title=ECEarth%20Tasmax%20test&CRS=EPSG:4326&';
config.MetadataServiceUrl = '&service=metadata&request=getmetadata';

//config.frontendContentURL = 'http://C4I_FRONTEND_CONTENT_S3_DNS';
//// C4I_FRONTEND_CONTENT_S3_DNS .. will be replaced by CI/CD in gitlab
//
//
