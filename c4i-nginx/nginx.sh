#!/bin/sh
source /etc/envvars
envsubst '\$SSL_DOMAINS \$SSL_FORWARD \$C4I_FRONTEND_BUCKET \$C4I_CONTENT_BUCKET' < /nginx.conf > /etc/nginx/nginx.conf
exec /usr/sbin/nginx -g "daemon off;"
