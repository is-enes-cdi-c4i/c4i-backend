#/bin/bash


C4I_PORT=443
ADAGUC_DATA_DIR=/data/adaguc-autowms
ADAGUC_AUTOWMS_DIR=/data/adaguc-autowms
ADAGUC_DATASET_DIR=/data/adaguc-datasets
ADAGUCSERVICES_DIR=/data/adaguc-services/
C4I_FRONTEND_BUCKET="http://c4i-frontend-dev-temp.s3-website-eu-west-1.amazonaws.com/"
C4I_CONTENT_BUCKET="http://c4i-frontend-content-master-temp.s3-website-eu-west-1.amazonaws.com"


usage() { echo "Usage: $0 -p <port number> -e <external adress> -a <autowmsdir> -d <dataset dir> -f <datadir>" 1>&2; exit 1; }


while getopts ":e:p:h:a:d:f:u:" o; do
    case "${o}" in
        e)
            EXTERNALADDRESS=${OPTARG}
            ;;
        p)
            C4I_PORT=${OPTARG}
            ;;
        a)
            ADAGUC_AUTOWMS_DIR=${OPTARG}
            ;;
        d)
            ADAGUC_DATASET_DIR=${OPTARG}
            ;;
        f)
            ADAGUC_DATA_DIR=${OPTARG}
            ;;
        u)  
            ADAGUCSERVICES_DIR=${OPTARG}
            ;;
        h)
            usage
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

if [ -z "${EXTERNALADDRESS}" ]; then
  EXTERNALADDRESS="https://${HOSTNAME}/"
  if [ "${C4I_PORT}" != "443" ]; then
    EXTERNALADDRESS="https://${HOSTNAME}:${C4I_PORT}/"
  fi
fi


rm .env
echo "ADAGUC_HOME=$HOME" >> .env
echo "ADAGUC_DATA_DIR=${ADAGUC_DATA_DIR}" >> .env
echo "ADAGUC_AUTOWMS_DIR=${ADAGUC_AUTOWMS_DIR=}" >> .env
echo "ADAGUC_DATASET_DIR=${ADAGUC_DATASET_DIR}" >> .env
echo "ADAGUCSERVICES_DIR=${ADAGUCSERVICES_DIR}" >> .env
echo "C4I_PORT=${C4I_PORT}" >> .env
echo "EXTERNALADDRESS=${EXTERNALADDRESS}" >> .env
echo "EXTERNALHOSTNAME=${HOSTNAME}" >> .env
echo "C4I_FRONTEND_BUCKET=${C4I_FRONTEND_BUCKET}" >> .env
echo "C4I_CONTENT_BUCKET=${C4I_CONTENT_BUCKET}" >> .env
echo "############### env file ###############"
cat .env
echo "############### env file ###############"
echo "ADAGUC will be available on ${EXTERNALADDRESS}"
